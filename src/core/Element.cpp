/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
#include <El-lite.hpp>

namespace El {

template<>
string TypeName<bool>()
{ return string("bool"); }
template<>
string TypeName<char>()
{ return string("char"); }
template<>
string TypeName<char*>()
{ return string("char*"); }
template<>
string TypeName<const char*>()
{ return string("const char*"); }
template<>
string TypeName<string>()
{ return string("string"); }
template<>
string TypeName<unsigned>()
{ return string("unsigned"); }
template<>
string TypeName<unsigned long>()
{ return string("unsigned long"); }
template<>
string TypeName<unsigned long long>()
{ return string("unsigned long long"); }
template<>
string TypeName<int>()
{ return string("int"); }
template<>
string TypeName<long int>()
{ return string("long int"); }
template<>
string TypeName<long long int>()
{ return string("long long int"); }
template<>
string TypeName<float>()
{ return string("float"); }
template<>
string TypeName<double>()
{ return string("double"); }
#ifdef EL_HAVE_QD
template<>
string TypeName<DoubleDouble>()
{ return string("DoubleDouble"); }
template<>
string TypeName<QuadDouble>()
{ return string("QuadDouble"); }
#endif
#ifdef EL_HAVE_QUAD
template<>
string TypeName<Quad>()
{ return string("Quad"); }
#endif
#ifdef EL_HAVE_GMPXX
template<>
string TypeName<BigInt>()
{ return string("BigInt"); }
template<>
string TypeName<BigFloat>()
{ return string("BigFloat"); }
#endif

// Basic element manipulation and I/O
// ==================================

// Pretty-printing
// ---------------

// TODO: Move into core/imports/quadmath.hpp?
#ifdef EL_HAVE_QUAD
ostream& operator<<( ostream& os, const Quad& alpha )
{
    char str[128];
    quadmath_snprintf( str, 128, "%Qe", alpha );
    os << str;
    return os;
}

istream& operator>>( istream& is, Quad& alpha )
{
    string token;
    is >> token; 
    alpha = strtoflt128( token.c_str(), NULL );
    return is;
}
#endif

// Conjugate
// ---------
#ifdef EL_HAVE_QD
Complex<DoubleDouble> Conj( const Complex<DoubleDouble>& alpha )
{
    Complex<DoubleDouble> alphaConj;
    alphaConj.realPart = alpha.realPart; 
    alphaConj.imagPart = -alpha.imagPart;
    return alphaConj;
}
Complex<QuadDouble> Conj( const Complex<QuadDouble>& alpha )
{
    Complex<QuadDouble> alphaConj;
    alphaConj.realPart = alpha.realPart; 
    alphaConj.imagPart = -alpha.imagPart;
    return alphaConj;
}

void Conj
( const Complex<DoubleDouble>& alpha,
        Complex<DoubleDouble>& alphaConj )
{
    alphaConj.realPart = alpha.realPart; 
    alphaConj.imagPart = -alpha.imagPart;
}
void Conj
( const Complex<QuadDouble>& alpha,
        Complex<QuadDouble>& alphaConj )
{
    alphaConj.realPart = alpha.realPart; 
    alphaConj.imagPart = -alpha.imagPart;
}
#endif
#ifdef EL_HAVE_GMPXX
Complex<BigFloat> Conj( const Complex<BigFloat>& alpha )
{
    Complex<BigFloat> alphaConj;
    Conj( alpha, alphaConj );
    return alphaConj;
}

void Conj( const Complex<BigFloat>& alpha, Complex<BigFloat>& alphaConj )
{
    alphaConj.real(alpha.real());
    alphaConj.imag(-alpha.imag());
}
#endif

// Return the complex argument
// ---------------------------
#ifdef EL_HAVE_QUAD
template<>
Quad Arg( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();

    return cargq(alpha);
}
#endif
#ifdef EL_HAVE_GMPXX
template<>
BigFloat Arg( const Complex<BigFloat>& alpha )
{
    throw std::runtime_error("Arg(Complex<BigFloat>) not implemented");
}
#endif

// Construct a complex number from its polar coordinates
// -----------------------------------------------------
#ifdef EL_HAVE_QD
Complex<DoubleDouble>
ComplexFromPolar( const DoubleDouble& r, const DoubleDouble& theta )
{
    const DoubleDouble realPart = r*Cos(theta);
    const DoubleDouble imagPart = r*Sin(theta);
    return Complex<DoubleDouble>(realPart,imagPart);
}
Complex<QuadDouble>
ComplexFromPolar( const QuadDouble& r, const QuadDouble& theta )
{
    const QuadDouble realPart = r*Cos(theta);
    const QuadDouble imagPart = r*Sin(theta);
    return Complex<QuadDouble>(realPart,imagPart);
}
#endif
#ifdef EL_HAVE_QUAD
Complex<Quad> ComplexFromPolar( const Quad& r, const Quad& theta )
{
    const Quad realPart = r*cosq(theta);
    const Quad imagPart = r*sinq(theta);
    return Complex<Quad>(realPart,imagPart);
}
#endif
#ifdef EL_HAVE_GMPXX
Complex<BigFloat> ComplexFromPolar( const BigFloat& r, const BigFloat& theta )
{
    throw std::runtime_error("ComplexFromPolar(BigFloat,BigFloat) not implemented");
}
#endif

// Magnitude and sign
// ==================
#ifdef EL_HAVE_QD
DoubleDouble Abs( const DoubleDouble& alpha ) EL_NO_EXCEPT
{ return fabs(alpha); }

QuadDouble Abs( const QuadDouble& alpha ) EL_NO_EXCEPT
{ return fabs(alpha); }

DoubleDouble Abs( const Complex<DoubleDouble>& alpha ) EL_NO_EXCEPT
{
    DoubleDouble realPart=alpha.realPart, imagPart=alpha.imagPart;
    const DoubleDouble scale = Max(Abs(realPart),Abs(imagPart));
    if( scale == DoubleDouble(0) )
        return scale;
    realPart /= scale;
    imagPart /= scale;
    return scale*Sqrt(realPart*realPart + imagPart*imagPart);
}

QuadDouble Abs( const Complex<QuadDouble>& alpha ) EL_NO_EXCEPT
{
    QuadDouble realPart=alpha.realPart, imagPart=alpha.imagPart;
    const QuadDouble scale = Max(Abs(realPart),Abs(imagPart));
    if( scale == QuadDouble(0) )
        return scale;
    realPart /= scale;
    imagPart /= scale;
    return scale*Sqrt(realPart*realPart + imagPart*imagPart);
}
#endif

#ifdef EL_HAVE_QUAD
Quad Abs( const Quad& alpha ) EL_NO_EXCEPT { return fabsq(alpha); }

Quad Abs( const Complex<Quad>& alphaPre ) EL_NO_EXCEPT
{ 
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    return cabsq(alpha); 
}
#endif

#ifdef EL_HAVE_GMPXX
BigInt Abs( const BigInt& alpha ) EL_NO_EXCEPT
{
    BigInt absAlpha;
    absAlpha.SetMinBits( alpha.NumBits() );
    mpz_abs( absAlpha.Pointer(), alpha.LockedPointer() );
    return absAlpha;
}

BigFloat Abs( const BigFloat& alpha ) EL_NO_EXCEPT
{
    BigFloat absAlpha(alpha);
    absAlpha.gmp_float=abs(alpha.gmp_float);
    return absAlpha;
}

BigFloat Abs( const Complex<BigFloat>& alpha ) EL_NO_EXCEPT
{
    BigFloat result;
    result.gmp_float=hypot(alpha.real().gmp_float, alpha.imag().gmp_float);
    return result;
}
#endif

#ifdef EL_HAVE_GMPXX
BigInt Sgn( const BigInt& alpha, bool symmetric ) EL_NO_EXCEPT
{
    const int numBits = alpha.NumBits();
    const int result = mpz_cmp_si( alpha.LockedPointer(), 0 );
    if( result < 0 )
        return BigInt(-1,numBits);
    else if( result > 0 || !symmetric )
        return BigInt(1,numBits);
    else
        return BigInt(0,numBits);
}

BigFloat Sgn( const BigFloat& alpha, bool symmetric ) EL_NO_EXCEPT
{
    mp_bitcnt_t prec = alpha.Precision();
    auto sign = alpha.Sign();
    if( sign < 0 )
        return BigFloat(-1,prec);
    else if( sign > 0 || !symmetric )
        return BigFloat(1,prec);
    else
        return BigFloat(0,prec);
}
#endif

// Exponentiation
// ==============
double Exp( const Int& alpha ) EL_NO_EXCEPT { return std::exp(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Exp( const DoubleDouble& alpha ) EL_NO_EXCEPT
{ return exp(alpha); }

QuadDouble Exp( const QuadDouble& alpha ) EL_NO_EXCEPT
{ return exp(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Exp( const Quad& alpha ) EL_NO_EXCEPT { return expq(alpha); }

Complex<Quad> Exp( const Complex<Quad>& alphaPre ) EL_NO_EXCEPT
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();

    __complex128 alphaExp = cexpq(alpha);
    return Complex<Quad>(crealq(alphaExp),cimagq(alphaExp)); 
}
#endif

#ifdef EL_HAVE_GMPXX
BigFloat Exp( const BigFloat& alpha ) EL_NO_EXCEPT
{
    throw std::runtime_error("Exp(BigFloat) not implemented");
}

Complex<BigFloat> Exp( const Complex<BigFloat>& alpha ) EL_NO_EXCEPT
{
    throw std::runtime_error("Exp(Complex<BigFloat>) not implemented");
}
#endif

#ifdef EL_HAVE_QD
// The QD implementation of 'pow' appears to have severe bugs when raising
// non-positive numbers to non-integer powers, as the implementation is:
//
//   dd_real pow(const dd_real &a, const dd_real &b) {
//       return exp(b * log(a));
//   }
//
DoubleDouble Pow( const DoubleDouble& alpha, const DoubleDouble& beta )
{
    if( alpha > DoubleDouble(0) )
    {
        return Exp( beta * Log(alpha) );
    }
    else if( alpha == DoubleDouble(0) )
    {
        return DoubleDouble(0);
    }
    else
    {
        RuntimeError(alpha,"^",beta," not yet supported");
        return DoubleDouble(0);
    }
}
DoubleDouble Pow( const DoubleDouble& alpha, const int& beta )
{ return pow(alpha,beta); }

QuadDouble Pow( const QuadDouble& alpha, const QuadDouble& beta )
{
    if( alpha > QuadDouble(0) )
    {
        return Exp( beta * Log(alpha) );
    }
    else if( alpha == QuadDouble(0) )
    {
        return QuadDouble(0);
    }
    else
    {
        RuntimeError(alpha,"^",beta," not yet supported");
        return QuadDouble(0);
    }
}
QuadDouble Pow( const QuadDouble& alpha, const int& beta )
{ return pow(alpha,beta); }
#endif

#ifdef EL_HAVE_QUAD
Quad Pow( const Quad& alpha, const Quad& beta )
{ return powq(alpha,beta); }

Quad Pow( const Quad& alpha, const Int& beta )
{ return powq(alpha,Quad(beta)); }

Complex<Quad> Pow( const Complex<Quad>& alphaPre, const Complex<Quad>& betaPre )
{
    __complex128 alpha, beta;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    __real__(beta)  = betaPre.real();
    __imag__(beta)  = betaPre.imag();

    __complex128 gamma = cpowq(alpha,beta);
    return Complex<Quad>(crealq(gamma),cimagq(gamma));
}

Complex<Quad> Pow( const Complex<Quad>& alphaPre, const Quad& betaPre )
{
    __complex128 alpha, beta;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    __real__(beta)  = betaPre;
    __imag__(beta)  = 0;

    __complex128 gamma = cpowq(alpha,beta);
    return Complex<Quad>(crealq(gamma),cimagq(gamma));
}

Complex<Quad> Pow( const Complex<Quad>& alpha, const Int& beta )
{
    return Pow( alpha, Quad(beta) );
}
#endif

#ifdef EL_HAVE_GMPXX
void Pow( const BigInt& alpha, const BigInt& beta, BigInt& gamma )
{
    mpz_pow_ui( gamma.Pointer(), alpha.LockedPointer(), (unsigned long)(beta) );
}

void Pow( const BigInt& alpha, const unsigned& beta, BigInt& gamma )
{
    mpz_pow_ui( gamma.Pointer(), alpha.LockedPointer(), beta );
}

void Pow( const BigInt& alpha, const unsigned long& beta, BigInt& gamma )
{
    mpz_pow_ui( gamma.Pointer(), alpha.LockedPointer(), beta );
}

void Pow( const BigInt& alpha, const unsigned long long& beta, BigInt& gamma )
{
    EL_DEBUG_ONLY(
      if( beta > static_cast<unsigned long long>(ULONG_MAX) )
      {
          RuntimeError("Excessively large exponent for Pow: ",beta); 
      }
    )
    unsigned long betaLong = static_cast<unsigned long>(beta);
    mpz_pow_ui( gamma.Pointer(), alpha.LockedPointer(), betaLong );
}

BigInt Pow( const BigInt& alpha, const BigInt& beta )
{
    BigInt gamma;
    Pow( alpha, beta, gamma );
    return gamma;
}

BigInt Pow( const BigInt& alpha, const unsigned& beta )
{
    BigInt gamma;
    Pow( alpha, beta, gamma );
    return gamma;
}

BigInt Pow( const BigInt& alpha, const unsigned long& beta )
{
    BigInt gamma;
    Pow( alpha, beta, gamma );
    return gamma;
}

BigInt Pow( const BigInt& alpha, const unsigned long long& beta )
{
    BigInt gamma;
    Pow( alpha, beta, gamma );
    return gamma;
}

void Pow( const BigFloat& alpha, const BigFloat& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,BigFloat,BigFloat) not implemented");
}

void Pow( const BigFloat& alpha, const unsigned& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,unsigned,BigFloat) not implemented");
}

void Pow( const BigFloat& alpha, const unsigned long& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,unsigned long,BigFloat) not implemented");
}

void Pow
( const BigFloat& alpha, const unsigned long long& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,unsigned long long,BigFloat) not implemented");
}

void Pow( const BigFloat& alpha, const int& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,int,BigFloat) not implemented");
}

void Pow( const BigFloat& alpha, const long int& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,long,BigFloat) not implemented");
}

void Pow( const BigFloat& alpha, const long long int& beta, BigFloat& gamma )
{
    throw std::runtime_error("Pow(BigFloat,long long,BigFloat) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const BigFloat& beta )
{
  if(beta==BigFloat(0.25))
    {
      return Sqrt(Sqrt(alpha));
    }
  else if(beta==BigFloat(-0.125))
    {
      return 1/Sqrt(Sqrt(Sqrt(alpha)));
    }
  else if(beta==BigFloat(2))
    {
      return alpha*alpha;
    }
  std::stringstream ss;
  ss << "Pow(BigFloat,BigFloat) not implemented for Pow("
     << alpha << ", "
     << beta << ")";
  throw std::runtime_error(ss.str());
}

Complex<BigFloat>
Pow( const Complex<BigFloat>& alpha, const Complex<BigFloat>& beta )
{
    throw std::runtime_error("Pow(Complex<BigFloat>,Complex<BigFloat>) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const unsigned& beta )
{
    throw std::runtime_error("Pow(BigFloat,unsigned) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const unsigned long& beta )
{
    throw std::runtime_error("Pow(BigFloat,unsigned long) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const unsigned long long& beta )
{
    throw std::runtime_error("Pow(BigFloat,unsigned long long) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const int& beta )
{
    throw std::runtime_error("Pow(BigFloat,int) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const long int& beta )
{
    throw std::runtime_error("Pow(BigFloat,long) not implemented");
}

BigFloat Pow( const BigFloat& alpha, const long long int& beta )
{
    throw std::runtime_error("Pow(BigFloat,long long) not implemented");
}

Complex<BigFloat>
Pow( const Complex<BigFloat>& alpha, const BigFloat& beta )
{
    throw std::runtime_error("Pow(Complex<BigFloat>,BigFloat) not implemented");
}
#endif

// Inverse exponentiation
// ----------------------
#ifdef EL_HAVE_QD
DoubleDouble Log( const DoubleDouble& alpha )
{ return log(alpha); }

QuadDouble Log( const QuadDouble& alpha )
{ return log(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Log( const Quad& alpha ) { return logq(alpha); }

Complex<Quad> Log( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();

    __complex128 logAlpha = clogq(alpha);
    return Complex<Quad>(crealq(logAlpha),cimagq(logAlpha));
}
#endif

#ifdef EL_HAVE_GMPXX
BigFloat Log( const BigFloat& alpha )
{
    throw std::runtime_error("Log(BigFloat) not implemented");
}

Complex<BigFloat> Log( const Complex<BigFloat>& alpha )
{
    throw std::runtime_error("Log(Complex<BigFloat>) not implemented");
}
#endif

#ifdef EL_HAVE_QD
DoubleDouble Log2( const DoubleDouble& alpha )
{ return Log(alpha)/DoubleDouble(dd_real::_log2); }

QuadDouble Log2( const QuadDouble& alpha )
{ return Log(alpha)/QuadDouble(qd_real::_log2); }

Complex<DoubleDouble> Log2( const Complex<DoubleDouble>& alpha )
{ return Log(alpha)/DoubleDouble(dd_real::_log2); }

Complex<QuadDouble> Log2( const Complex<QuadDouble>& alpha )
{ return Log(alpha)/QuadDouble(qd_real::_log2); }
#endif

#ifdef EL_HAVE_QUAD
Quad Log2( const Quad& alpha )
{ return log2q(alpha); }
#endif

#ifdef EL_HAVE_GMPXX
BigFloat Log2( const BigFloat& alpha )
{
    throw std::runtime_error("Log2(BigFloat) not implemented");
}

Complex<BigFloat> Log2( const Complex<BigFloat>& alpha )
{
    throw std::runtime_error("Log2(Complex<BigFloat>) not implemented");
}
#endif

#ifdef EL_HAVE_QD
DoubleDouble Log10( const DoubleDouble& alpha )
{ return Log(alpha)/DoubleDouble(dd_real::_log10); }

QuadDouble Log10( const QuadDouble& alpha )
{ return Log(alpha)/QuadDouble(qd_real::_log10); }

Complex<DoubleDouble> Log10( const Complex<DoubleDouble>& alpha )
{ return Log(alpha)/DoubleDouble(dd_real::_log10); }

Complex<QuadDouble> Log10( const Complex<QuadDouble>& alpha )
{ return Log(alpha)/QuadDouble(qd_real::_log10); }
#endif

#ifdef EL_HAVE_QUAD
Quad Log10( const Quad& alpha )
{ return log10q(alpha); }
#endif


template<>
Int Sqrt( const Int& alpha ) { return Int(sqrt(alpha)); }

#ifdef EL_HAVE_QD
DoubleDouble Sqrt( const DoubleDouble& alpha ) { return sqrt(alpha); }

QuadDouble Sqrt( const QuadDouble& alpha ) { return sqrt(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Sqrt( const Quad& alpha ) { return sqrtq(alpha); }

Complex<Quad> Sqrt( const Complex<Quad>& alphaPre )
{ 
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();

    __complex128 sqrtAlpha = csqrtq(alpha);
    return Complex<Quad>(crealq(sqrtAlpha),cimagq(sqrtAlpha));
}
#endif

#ifdef EL_HAVE_GMPXX
void Sqrt( const BigInt& alpha, BigInt& alphaSqrt )
{ mpz_sqrt( alphaSqrt.Pointer(), alpha.LockedPointer() ); }

void Sqrt( const BigFloat& alpha, BigFloat& alphaSqrt )
{
    alphaSqrt.gmp_float=sqrt(alpha.gmp_float);
}

void Sqrt( const Complex<BigFloat>& alpha, Complex<BigFloat>& alphaSqrt )
{
    throw std::runtime_error("Sqrt(Complex<BigFloat>,Complex<BigFloat>) not implemented");
}

BigInt Sqrt( const BigInt& alpha )
{
    BigInt alphaSqrt;
    Sqrt( alpha, alphaSqrt );
    return alphaSqrt;
}

BigFloat Sqrt( const BigFloat& alpha )
{
    BigFloat alphaSqrt;
    alphaSqrt.SetPrecision( alpha.Precision() );
    Sqrt( alpha, alphaSqrt );
    return alphaSqrt;
}

Complex<BigFloat> Sqrt( const Complex<BigFloat>& alpha )
{
    Complex<BigFloat> alphaSqrt;
    alphaSqrt.SetPrecision( alpha.Precision() );
    Sqrt( alpha, alphaSqrt );
    return alphaSqrt;
}
#endif

template<>
unsigned ISqrt( const unsigned& alpha )
{ return static_cast<unsigned>(std::sqrt(alpha)); }
template<>
unsigned long ISqrt( const unsigned long& alpha )
{ return static_cast<unsigned long>(std::sqrt(alpha)); }
template<>
int ISqrt( const int& alpha )
{ return static_cast<int>(std::sqrt(alpha)); }
template<>
long int ISqrt( const long int& alpha )
{ return static_cast<long int>(std::sqrt(alpha)); }

#ifdef EL_HAVE_GMPXX
void ISqrt( const BigInt& alpha, BigInt& alphaSqrt )
{
    mpz_sqrt( alphaSqrt.Pointer(), alpha.LockedPointer() );
}

template<>
BigInt ISqrt( const BigInt& alpha )
{
    BigInt alphaSqrt;
    ISqrt( alpha, alphaSqrt );
    return alphaSqrt;
}
#endif

// Trigonometric
// =============
double Cos( const Int& alpha ) { return std::cos(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Cos( const DoubleDouble& alpha ) { return cos(alpha); }

QuadDouble Cos( const QuadDouble& alpha ) { return cos(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Cos( const Quad& alpha ) { return cosq(alpha); }

Complex<Quad> Cos( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 cosAlpha = ccosq(alpha);
    return Complex<Quad>(crealq(cosAlpha),cimagq(cosAlpha));
}
#endif

#ifdef EL_HAVE_GMPXX
BigFloat Cos( const BigFloat& alpha )
{
    throw std::runtime_error("Cos(BigFloat) not implemented");
}

Complex<BigFloat> Cos( const Complex<BigFloat>& alpha )
{
    throw std::runtime_error("Cos(Complex<BigFloat>) not implemented");
}
#endif

double Sin( const Int& alpha ) { return std::sin(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Sin( const DoubleDouble& alpha ) { return sin(alpha); }

QuadDouble Sin( const QuadDouble& alpha ) { return sin(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Sin( const Quad& alpha ) { return sinq(alpha); }

Complex<Quad> Sin( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 sinAlpha = csinq(alpha);
    return Complex<Quad>(crealq(sinAlpha),cimagq(sinAlpha));
}
#endif

#ifdef EL_HAVE_GMPXX
BigFloat Sin( const BigFloat& alpha )
{
    throw std::runtime_error("Sin(BigFloat) not implemented");
}

Complex<BigFloat> Sin( const Complex<BigFloat>& alpha )
{
    throw std::runtime_error("Sin(Complex<BigFloat>) not implemented");
}
#endif

double Tan( const Int& alpha ) { return std::tan(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Tan( const DoubleDouble& alpha ) { return tan(alpha); }

QuadDouble Tan( const QuadDouble& alpha ) { return tan(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Tan( const Quad& alpha ) { return tanq(alpha); }

Complex<Quad> Tan( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 tanAlpha = ctanq(alpha);
    return Complex<Quad>(crealq(tanAlpha),cimagq(tanAlpha));
}
#endif

// Inverse trigonometric
// ---------------------
double Acos( const Int& alpha ) { return std::acos(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Acos( const DoubleDouble& alpha ) { return acos(alpha); }

QuadDouble Acos( const QuadDouble& alpha ) { return acos(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Acos( const Quad& alpha ) { return acosq(alpha); }

Complex<Quad> Acos( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 acosAlpha = cacosq(alpha);
    return Complex<Quad>(crealq(acosAlpha),cimagq(acosAlpha));
}
#endif

double Asin( const Int& alpha ) { return std::asin(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Asin( const DoubleDouble& alpha ) { return asin(alpha); }

QuadDouble Asin( const QuadDouble& alpha ) { return asin(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Asin( const Quad& alpha ) { return asinq(alpha); }

Complex<Quad> Asin( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 asinAlpha = casinq(alpha);
    return Complex<Quad>(crealq(asinAlpha),cimagq(asinAlpha));
}
#endif

double Atan( const Int& alpha ) { return std::atan(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Atan( const DoubleDouble& alpha ) { return atan(alpha); }

QuadDouble Atan( const QuadDouble& alpha ) { return atan(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Atan( const Quad& alpha ) { return atanq(alpha); }

Complex<Quad> Atan( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 atanAlpha = catanq(alpha);
    return Complex<Quad>(crealq(atanAlpha),cimagq(atanAlpha));
}
#endif

#ifdef EL_HAVE_GMPXX
BigFloat Atan( const BigFloat& alpha )
{
    throw std::runtime_error("Atan(BigFloat) not implemented");
}

Complex<BigFloat> Atan( const Complex<BigFloat>& alpha )
{
    throw std::runtime_error("Atan(Complex<BigFloat>) not implemented");
}
#endif

double Atan2( const Int& y, const Int& x ) { return std::atan2( y, x ); }

#ifdef EL_HAVE_QD
DoubleDouble Atan2( const DoubleDouble& y, const DoubleDouble& x )
{ return atan2(y,x); }

QuadDouble Atan2( const QuadDouble& y, const QuadDouble& x )
{ return atan2(y,x); }
#endif

#ifdef EL_HAVE_QUAD
Quad Atan2( const Quad& y, const Quad& x ) { return atan2q( y, x ); }
#endif

// Hyperbolic
// ==========
double Cosh( const Int& alpha ) { return std::cosh(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Cosh( const DoubleDouble& alpha ) { return cosh(alpha); }

QuadDouble Cosh( const QuadDouble& alpha ) { return cosh(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Cosh( const Quad& alpha ) { return coshq(alpha); }

Complex<Quad> Cosh( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 coshAlpha = ccoshq(alpha);
    return Complex<Quad>(crealq(coshAlpha),cimagq(coshAlpha));
}
#endif

double Sinh( const Int& alpha ) { return std::sinh(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Sinh( const DoubleDouble& alpha ) { return sinh(alpha); }

QuadDouble Sinh( const QuadDouble& alpha ) { return sinh(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Sinh( const Quad& alpha ) { return sinhq(alpha); }

Complex<Quad> Sinh( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 sinhAlpha = csinhq(alpha);
    return Complex<Quad>(crealq(sinhAlpha),cimagq(sinhAlpha));
}
#endif

double Tanh( const Int& alpha ) { return std::tanh(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Tanh( const DoubleDouble& alpha ) { return tanh(alpha); }

QuadDouble Tanh( const QuadDouble& alpha ) { return tanh(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Tanh( const Quad& alpha ) { return tanhq(alpha); }

Complex<Quad> Tanh( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 tanhAlpha = ctanhq(alpha);
    return Complex<Quad>(crealq(tanhAlpha),cimagq(tanhAlpha));
}
#endif

// Inverse hyperbolic
// ------------------
double Acosh( const Int& alpha ) { return std::acosh(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Acosh( const DoubleDouble& alpha ) { return acosh(alpha); }

QuadDouble Acosh( const QuadDouble& alpha ) { return acosh(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Acosh( const Quad& alpha ) { return acoshq(alpha); }

Complex<Quad> Acosh( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 acoshAlpha = cacoshq(alpha);
    return Complex<Quad>(crealq(acoshAlpha),cimagq(acoshAlpha));
}
#endif

double Asinh( const Int& alpha ) { return std::asinh(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Asinh( const DoubleDouble& alpha ) { return asinh(alpha); }

QuadDouble Asinh( const QuadDouble& alpha ) { return asinh(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Asinh( const Quad& alpha ) { return asinhq(alpha); }

Complex<Quad> Asinh( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 asinhAlpha = casinhq(alpha);
    return Complex<Quad>(crealq(asinhAlpha),cimagq(asinhAlpha));
}
#endif

double Atanh( const Int& alpha ) { return std::atanh(alpha); }

#ifdef EL_HAVE_QD
DoubleDouble Atanh( const DoubleDouble& alpha ) { return atanh(alpha); }

QuadDouble Atanh( const QuadDouble& alpha ) { return atanh(alpha); }
#endif

#ifdef EL_HAVE_QUAD
Quad Atanh( const Quad& alpha ) { return atanhq(alpha); }

Complex<Quad> Atanh( const Complex<Quad>& alphaPre )
{
    __complex128 alpha;
    __real__(alpha) = alphaPre.real();
    __imag__(alpha) = alphaPre.imag();
    
    __complex128 atanhAlpha = catanhq(alpha);
    return Complex<Quad>(crealq(atanhAlpha),cimagq(atanhAlpha));
}
#endif

// Rounding
// ========

// Round to the nearest integer
// ----------------------------
template<>
Int Round( const Int& alpha ) { return alpha; }
#ifdef EL_HAVE_QD
template<>
DoubleDouble Round( const DoubleDouble& alpha ) { return nint(alpha); }
template<>
QuadDouble Round( const QuadDouble& alpha ) { return nint(alpha); }
#endif
#ifdef EL_HAVE_QUAD
template<>
Quad Round( const Quad& alpha ) { return rintq(alpha); }
#endif
#ifdef EL_HAVE_GMPXX
template<>
BigInt Round( const BigInt& alpha ) { return alpha; }
template<>
BigFloat Round( const BigFloat& alpha )
{ 
    BigFloat alphaRound;
    alphaRound.gmp_float=trunc(alpha.gmp_float);
    return alphaRound;
}
#endif

// Ceiling
// -------
template<> Int Ceil( const Int& alpha ) { return alpha; }
#ifdef EL_HAVE_QD
template<> DoubleDouble Ceil( const DoubleDouble& alpha )
{ return ceil(alpha); }
template<> QuadDouble Ceil( const QuadDouble& alpha )
{ return ceil(alpha); }
#endif
#ifdef EL_HAVE_QUAD
template<> Quad Ceil( const Quad& alpha ) { return ceilq(alpha); }
#endif
#ifdef EL_HAVE_GMPXX
template<>
BigInt Ceil( const BigInt& alpha ) { return alpha; }
template<>
BigFloat Ceil( const BigFloat& alpha )
{
    BigFloat alphaCeil(alpha);
    alphaCeil.gmp_float=ceil(alpha.gmp_float);
    return alphaCeil;
}
#endif

// Floor
// -----
template<> Int Floor( const Int& alpha ) { return alpha; }
#ifdef EL_HAVE_QD
template<> DoubleDouble Floor( const DoubleDouble& alpha )
{ return floor(alpha); }
template<> QuadDouble Floor( const QuadDouble& alpha )
{ return floor(alpha); }
#endif
#ifdef EL_HAVE_QUAD
template<> Quad Floor( const Quad& alpha ) { return floorq(alpha); }
#endif
#ifdef EL_HAVE_GMPXX
template<>
BigInt Floor( const BigInt& alpha ) { return alpha; }
template<> BigFloat Floor( const BigFloat& alpha )
{
    BigFloat alphaFloor(alpha);
    alphaFloor.gmp_float=floor(alpha.gmp_float);
    return alphaFloor;
}
#endif

// Pi
// ==
#ifdef EL_HAVE_QD
template<> DoubleDouble Pi<DoubleDouble>() { return dd_real::_pi; }
template<> QuadDouble Pi<QuadDouble>() { return qd_real::_pi; }
#endif
#ifdef EL_HAVE_QUAD
template<> Quad Pi<Quad>() { return M_PIq; }
#endif
#ifdef EL_HAVE_GMPXX
template<>
BigFloat Pi<BigFloat>()
{
    throw std::runtime_error("Pi<BigFloat> not implemented");
}
#endif

// Gamma
// =====
#ifdef EL_HAVE_QD
template<>
DoubleDouble Gamma( const DoubleDouble& alpha )
{
    // TODO: Decide a more precise means of handling this
    return Gamma(to_double(alpha));
}
template<>
QuadDouble Gamma( const QuadDouble& alpha )
{
    // TODO: Decide a more precise means of handling this
    return Gamma(to_double(alpha));
}
#endif
#ifdef EL_HAVE_QUAD
template<>
Quad Gamma( const Quad& alpha ) { return tgammaq(alpha); }
#endif

#ifdef EL_HAVE_QD
template<>
DoubleDouble LogGamma( const DoubleDouble& alpha )
{
    // TODO: Decide a more precise means of handling this
    return LogGamma(to_double(alpha));
}
template<>
QuadDouble LogGamma( const QuadDouble& alpha )
{
    // TODO: Decide a more precise means of handling this
    return LogGamma(to_double(alpha));
}
#endif
#ifdef EL_HAVE_QUAD
template<>
Quad LogGamma( const Quad& alpha ) { return lgammaq(alpha); }
#endif

} // namespace El
