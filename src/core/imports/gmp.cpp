/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
#include <El-lite.hpp>
#ifdef EL_HAVE_GMPXX

namespace {
El::BigInt bigIntZero, bigIntOne, bigIntTwo;
}

namespace El {

const BigInt& BigIntZero() { return ::bigIntZero; }
const BigInt& BigIntOne()  { return ::bigIntOne;  }
const BigInt& BigIntTwo()  { return ::bigIntTwo;  }

namespace gmp {

size_t num_limbs;
int numIntLimbs;
  
void SetMinIntBits( int numBits )
{ 
    static bool previouslySet = false;
    const int numIntLimbsNew = (numBits+(GMP_NUMB_BITS-1)) / GMP_NUMB_BITS;
    if( previouslySet && numIntLimbs == numIntLimbsNew ) 
        return;

    if( previouslySet )
        mpi::DestroyBigIntFamily();
    numIntLimbs = numIntLimbsNew;
    mpi::CreateBigIntFamily();

    ::bigIntZero.SetNumLimbs( num_limbs );
    ::bigIntOne.SetNumLimbs( num_limbs );
    ::bigIntTwo.SetNumLimbs( num_limbs );
    ::bigIntZero = 0;
    ::bigIntOne = 1;
    ::bigIntTwo = 2;

    previouslySet = true;
}

int NumIntBits()
{ return numIntLimbs*GMP_NUMB_BITS; }

int NumIntLimbs()
{ return numIntLimbs; }


mp_bitcnt_t Precision()
{ return mpf_get_default_prec(); }

void SetPrecision( mp_bitcnt_t prec )
{
    static bool previouslySet = false;
    if(previouslySet)
      {
        throw std::runtime_error("Not allowed to call SetPrecision twice");
      }
    if( previouslySet && prec == mpf_get_default_prec() )
        return;

    // This is the same calculation as used in gmp.  See
    //    set_prc.c::mpf_set_prec()
    //    gmp-impl.h::__GMPF_BITS_TO_PREC)
    num_limbs=(std::max(prec,static_cast<mp_bitcnt_t>(53)) + 2*GMP_NUMB_BITS - 1)/GMP_NUMB_BITS + 1;
    if( previouslySet )
        mpi::DestroyBigFloatFamily();
    mpf_set_default_prec( prec ); 
    mpi::CreateBigFloatFamily();

    previouslySet = true;
}

}

}

#endif // ifdef EL_HAVE_GMPXX
